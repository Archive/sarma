#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""docstorage defines a set of object for accessing documents in a database."""

from sqlobject import * 
import datetime

conn = connectionForURI('sqlite:/home/danilo/public_html/sarma/database.db')
#conn = connectionForURI('mysql://username:pw@localhost/database')
__connection__ = conn

class Document(SQLObject):
    _cacheValue = False

    Type = StringCol()
    Source = StringCol()
    Destination = StringCol()
    Language = ForeignKey('Language', notNone=True) # "en_US" for English US, "en_GB" for British English
    BaseUnit = ForeignKey('Unit')

    Versions = MultipleJoin('DocumentTag')
    Privileges = MultipleJoin('UserPrivileges')

        

class DocumentTag(SQLObject):
    _cacheValue = False

    Document = ForeignKey('Document')
    Tag = UnicodeCol() # descriptive tag name, eg. "gnome-2-12" or "gimp-2.4"
    Description = UnicodeCol() # human readable tag name, such as "GNOME 2.12 Administration Guide" or "Gimp 2.4 User Manual"
    Date = DateTimeCol(default=datetime.datetime.now)
    BaseUnitRevision = IntCol() # FIXME: what are we choosing for revision: int most likely
    Type = EnumCol(enumValues=['edition', 'revision', 'minor']) # Importance of this tag

class Unit(SQLObject):
    _cacheValue = False

    Document = ForeignKey('Document') # some redunancy for speedier work
    Descriptor = UnicodeCol()
    Type = EnumCol(enumValues=['string', 'struct', 'sequence'])
    Revision = IntCol()
    RelatedTo = ForeignKey('Unit', dbName = 'related_id', default=None) # either parent or untranslated unit
    Sequence = IntCol(default=0)
    Date = DateTimeCol(default=datetime.datetime.now)

    Tags = MultipleJoin('UnitTag')
    Changes = MultipleJoin('UnitChange')

    Comments = MultipleJoin('UnitComment')

    # FIXME: Make Data be either string, dictionary or array, depending on the type
    Data = MultipleJoin('UnitData')

    def _init(self, *args, **kw):
        SQLObject._init(self, *args, **kw)
        #if self.Type == 'struct':
        self.Values = { }
        for field in self.Data:
            self.Values[field.FieldName] = field.Content

class UnitTag(SQLObject):
    _cacheValue = False

    Unit = ForeignKey('Unit')
    Tag = StringCol()
    Date = DateTimeCol(default=datetime.datetime.now)

class UnitChange(SQLObject):
    _cacheValue = False

    Unit = ForeignKey('Unit')
    FromRevision = IntCol()
    NewRevision = IntCol()
    BaseRevision = IntCol()
    Approved = IntCol(default = 0) # 1 - yes, 0 - no
    Date = DateTimeCol(default=datetime.datetime.now)
    ChangeType = EnumCol(enumValues=['edit', 'insert-after', 'insert-before', 'move-up', 'move-down', 'remove'])
    Parameter = UnicodeCol()
    Editor = IntCol()

class UnitData(SQLObject):
    _cacheValue = False

    Unit = ForeignKey('Unit')
    FieldName = StringCol(default="TEXT")
    Content = UnicodeCol(default=None)
    Hints = StringCol(default=None) # rendering and editing hints, such as "imageurl", "table", "preformatted",...

class UnitComment(SQLObject):
    _cacheValue = False

    Unit = ForeignKey('Unit')
    Comment = UnicodeCol(default=None)
    
    Poster = ForeignKey('User', notNone=True) # Allow anonymous comments?
    DateTime = DateTimeCol(default=datetime.datetime.now)

class Translation(SQLObject):
    _cacheValue = False

    Original = UnicodeCol()
    Translation = UnicodeCol()
    #Language = ForeignKey('Language')

    # FIXME: Do we really need Document key?
    Document = ForeignKey('Document', default=None)
    Unit = ForeignKey('Unit', default=None)
    
    Contributor = ForeignKey('User', notNone=True)
    DateTime = DateTimeCol(default=datetime.datetime.now)
    Revision = IntCol()
    
class Language(SQLObject):
    _cacheValue = True

    Code = StringCol(length=20, alternateID=True)
    Name = UnicodeCol()

    Translators = RelatedJoin('User')

class UserPrivileges(SQLObject):
    Document = ForeignKey('Document', default = 0)
    User = ForeignKey('User')
    Reading = IntCol()
    Editing = IntCol() # Only allows in-place editting (i.e. simple bug-fixing such as spelling corrections, etc)
    Writing = IntCol() # Allows full editing (inserting, deleting units, rearranging stuff, etc.)
    Metadata = IntCol() # Allows metadata changes

class User(SQLObject):
    FullName = UnicodeCol()
    Email = UnicodeCol()
    Languages = RelatedJoin('Language')
    Password = UnicodeCol()

    Privileges = MultipleJoin('UserPrivileges')

class File:
    """Handles revisioned file storage, as related to documents and units."""
    def __init__(self, document, unit, revision, path = None):
        import os.path
        self.basepath = os.path.join(".", "files")
        self.document = document
        self.unit = unit
        self.revision = revision
        self.filename = os.path.join(basepath, str(document), str(unit)+"."+str(revision))
        
        if path:
            self.replace_file(path)
            
    def replace_file(self, with):
        import shutil
        shutil.copyfile(with, self.filename)
        
    
def init():
    Document.createTable(ifNotExists = True)
    DocumentTag.createTable(ifNotExists = True)
    Unit.createTable(ifNotExists = True)
    UnitTag.createTable(ifNotExists = True)
    UnitChange.createTable(ifNotExists = True)
    UnitData.createTable(ifNotExists = True)
    UnitComment.createTable(ifNotExists = True)
    UserPrivileges.createTable(ifNotExists = True)

    Language.createTable(ifNotExists = True)
    if not Language.selectBy(Code="en_US").count():
        en_US = Language(Code="en_US", Name="English (U.S.)")

    User.createTable(ifNotExists = True)
    admin = User(FullName="Administrator", Email="danilo@gnome.org", Password="adminpw")
    myprivs = UserPrivileges(User=admin, Reading=1, Editing=1, Writing=1, Metadata=1)
    demo = User(FullName="demo", Email="danilo@gnome.org", Password="demo")
    demoprivs = UserPrivileges(User=demo, Reading=1, Editing=1, Writing=1, Metadata=1)

    Translation.createTable(ifNotExists = True)

