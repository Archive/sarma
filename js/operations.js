var http = getHTTPObject(); // We create the HTTP Object

function handleHttpResponse() 
{ 
  if (http.readyState == 4) 
    { 
      // 
      results = http.responseText.split("\n", 2); 
      code = results[0];
      text = results[1];

      if (code!="0") {
	alert("Error: " + code + "\n\n" + text);
	return false;
      } else {
	//alert(text);
	return true;
      }
    }
}

function getHTTPObject() {
  var xmlhttp;
  /*@cc_on
  @if (@_jscript_version >= 5)
    try {
      xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (E) {
        xmlhttp = false;
      }
    }
  @else
  xmlhttp = false;
  @end @*/
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}

function saveContents(unitid, newcontents) {
  http.open("GET", "remote-calls.py?method=edit&unit="+unitid+"&value="+encodeURIComponent(newcontents), true); 
  http.onreadystatechange = handleHttpResponse; 
  http.send(null);
}


function setTag(unitid, tag) {
  http.open("GET", "remote-calls.py?method=settag&unit="+unitid+"&value="+encodeURIComponent(tag), true); 
  http.onreadystatechange = handleHttpResponse; 
  http.send(null);
}

function clearTag(unitid, tag) {
  http.open("GET", "remote-calls.py?method=cleartag&unit="+unitid+"&value="+encodeURIComponent(tag), true); 
  http.onreadystatechange = handleHttpResponse; 
  http.send(null);
}

function addComment(unitid, comment) {
  http.open("GET", "remote-calls.py?method=addcomment&unit="+unitid+"&value="+encodeURIComponent(comment), true); 
  http.onreadystatechange = handleHttpResponse; 
  http.send(null);
}
