/* -*- coding: utf-8 -*-
 *
 * Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
 *
 * This file is part of Sarma.
 *
 * Sarma is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Sarma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Sarma; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

var latestFrame;

function setFrameHtml(IFrameObj, myhtml) {
  if (!IFrameObj && !myhtml) {
    var IFrameObj = latestFrame;
    var myhtml = IFrameObj.html;
  }
  var IFrameDoc;

  if (navigator.userAgent.indexOf('Gecko') !=-1 
    && !IFrameObj.contentDocument && !IFrameObj.contentDocument.body) {
    // we have to give Gecko a fraction of a second
    // to recognize the new IFrame
    latestFrame = IFrameObj;
    setTimeout('setFrameHtml()', 500);
    return false;
  }
  
  if (IFrameObj.contentDocument) {
    // For Gecko
    //alert("OVDI!");
    IFrameDoc = IFrameObj.contentDocument; 
  } else if (IFrameObj.contentWindow) {
    // For IE5.5 and IE6
    IFrameDoc = IFrameObj.contentWindow.document;
  } else if (IFrameObj.document) {
    // For IE5
    IFrameDoc = IFrameObj.document;
  } else {
    return true;
  }
  
  if (IFrameDoc.body) {
    IFrameDoc.body.innerHTML = myhtml;
  }
  IFrameObj.style.height = IFrameObj.scrollHeight+2;
  IFrameDoc.designMode = "on";
  if (IFrameObj.contentWindow && IFrameObj.contentWindow.focus) {
    IFrameObj.contentWindow.focus();
  }
  return false;
}


function editUnit(id) {
  //alert("Editing "+id+"...");
    divId = "div_" + id;
    contentId = "content_" + id;
    editbarId = "editbar_" + id;
    frameId = "editor_iframe_" + id;
    div = document.getElementById(divId);
    content = document.getElementById(contentId);
    editbar = document.getElementById(editbarId);
    frame = document.getElementById(frameId);
    if (frame) {
      if (frame.contentWindow && frame.contentWindow.focus) {
	frame.contentWindow.focus();
      }
      return true;
    }
    if (div && content) {
      html = content.innerHTML;
      if (editbar) {
	editbar.style.display='block';
      }
      myframe = document.createElement("iframe");
      myframe.id = "editor_iframe_" + id;
      //myframe.style.position = 'absolute';
      //myframe.style.left = div.offsetLeft;
      //myframe.style.top = div.offsetTop;
      myframe.style.position = 'absolute';
      myframe.style.left = "-1em";
      myframe.style.top = "0px";
      myframe.style.height = content.offsetHeight - 2;
      myframe.style.width = div.offsetWidth;
      myframe.style.border = "1px dashed red";
      myframe.style.background = "white";
      myframe.style.overflow = "auto";
      //myframe.style.padding = "1em 0px 0px 0px";
      myframe.setAttribute("src", "templates/editor.html");
      content.parentNode.insertBefore(myframe, content.nextSibling);

      content.style.height = myframe.scrollHeight;
      myframe.style.height = content.offsetHeight - 2;

      editbar.style.margin = "5px";

      latestFrame = myframe;
      latestFrame.html = html;
      
      window.setTimeout('setFrameHtml(myframe, html)', 500);
      //window.setTimeout('setFrameHtml()', 500);
    }
    return true;
}


function removeUnit(id) {
  //alert("Removing "+id+"...");
    divId = "div_" + id;
    if (!confirm("You are permanently removing this unit from the document!")) {
      return true;
    }

    cancelEdit(id);
    div = document.getElementById(divId);

    if (div) {
      div.parentNode.removeChild(div);
      delete div;
    }
    return true;
}


function cancelEdit(id) {
  //alert("Restoring "+id+"...");
    divId = "div_" + id;
    frameId = "editor_iframe_" + id;
    contentId = "content_" + id;
    editbarId = "editbar_" + id;
    div = document.getElementById(divId);
    frame = document.getElementById(frameId);
    if (frame) {
      frame.style.display="none";
      frame.parentNode.removeChild(frame);
      delete frame;
    }
    content = document.getElementById(contentId);
    content.style.height = "auto";
    editbar = document.getElementById(editbarId);
    if (editbar) {
      editbar.style.display="none";
    }
    return true;
}


function saveEdit(id) {
  //alert("Saving "+id+"...");
    frameId = "editor_iframe_" + id;
    contentId = "content_" + id;
    frame = document.getElementById(frameId);
    frame.style.height = frame.scrollHeight;
    content = document.getElementById(contentId);
    content.style.height = "auto";
    content.innerHTML = frame.contentDocument.body.innerHTML;
    //alert(content.innerHTML);
    saveContents(id, content.innerHTML);
    cancelEdit(id);
    return true;
}




function editSourceUnit(id) {
    divId = "div_" + id;
    contentId = "content_" + id;
    editbarId = "editbar_" + id;
    editorId = "editor_" + id;
    div = document.getElementById(divId);
    content = document.getElementById(contentId);
    editbar = document.getElementById(editbarId);
    editor = document.getElementById(editorId);
    if (editor) {
      if (editbar) {
	editbar.style.display='block';
      }
      editor.removeAttribute("readonly");
      if (editor.focus) {
	editor.focus();
      }
      return true;
    }

    return true;
}

function cancelSourceEdit(id) {
    editorId = "editor_" + id;
    editbarId = "editbar_" + id;
    editor = document.getElementById(editorId);
    editor.setAttribute("readonly", "readonly");
    editbar = document.getElementById(editbarId);
    editbar.style.display="none";
    return true;
}


function saveSourceEdit(id) {
    cancelSourceEdit(id);
    return true;
}




function toggleSection(id) {
    divId = "div_" + id;
    imgId = "toggle_" + id;
    div = document.getElementById(divId);
    img = document.getElementById(imgId);
    if (!div) return true;
    if (div.style.display != 'none') {
      div.style.display = 'none';
      img.setAttribute("src","toolbar/stock_expand.png");
      img.setAttribute("title","Expand");
    } else {
      div.style.display = 'block';
      img.setAttribute("src","toolbar/stock_collapse.png");
      img.setAttribute("title","Collapse");
    }
    return true;
}


function setCssStyle(id, elementId, myclass) {
  frameId = "editor_iframe_" + id;
  frame = document.getElementById(frameId);
  element = document.getElementById(elementId+"_"+id);
  
  span = document.createElement("span");
  span.setAttribute("class", myclass);
  sel = frame.contentWindow.getSelection();
  if (sel.rangeCount) {
    rang = sel.getRangeAt(0);
    sel.removeAllRanges();
    cont = rang.extractContents();
    span.appendChild(cont);
    rang.insertNode(span);
  }
  return true;
}

function cleanFormatting(id) {
  frameId = "editor_iframe_" + id;
  frame = document.getElementById(frameId);
  frame.contentWindow.document.execCommand("removeformat", false, null);
  return true;
}


function toggleTag(input, id, tag) {
  //inputId = tag + "_" + id;
  //input = document.getElementById(inputId);
  if (input) {
    if (!input.checked) {
      clearTag(id, tag);
    } else { 
      setTag(id, tag);
    }
  } else {
    alert("Can't find myself, this shouldn't happen!");
  }
  return true;
}


function postComment(id) {
  input = prompt("Enter a comment you wish to add:");
  if (input) {
     addComment(id, input);
  }
  return true;
}
