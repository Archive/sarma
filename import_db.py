#!/usr/bin/env python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""Imports general DocBook documents into database."""

import libxml2
import docstorage
from common import *

DS = docstorage

class DocBookReader:
    def __init__(self, file=None, content=""):
        DS.init()

        if file:
            ctxt = libxml2.createFileParserCtxt(file)
            ctxt.lineNumbers(1)
        else:
            file = 'NONE'
            ctxt = libxml2.createDocParserCtxt(content.encode('utf-8'))

        ctxt.replaceEntities(1)
        ctxt.parseDocument()
        xmltree = ctxt.doc()
        root = xmltree.getRootElement()

        en_US = DS.Language.selectBy(Code="en_US")[0]
        DEBUG(repr(en_US))

        INFO(root.name)
        self.doc = DS.Document(Type = root.name,
                               Source = file,
                               Destination = 'cvs:///gnome/%s' % (file),
                               Language = en_US,
                               BaseUnit = 0)
        baseunit = DS.Unit( Document = self.doc,
                            Descriptor = root.name,
                            Type = 'struct',
                            Revision = 1,
                            RelatedTo = None )
        self.doc.BaseUnit = baseunit

        self.parse_book(root)

    def tag_is_inline(self, tag):
        """Returns true if a tag is "inline", i.e. serialized as part of other messages."""
        return tag in [ 'application', 'emphasis', 'filename', 'function', 'figure',
                        'guibutton', 'guilabel', 'guimenu', 'guimenuitem', 'guisubmenu',
                        'keycap', 'keycombo', 'literal', 'type', 'parameter', 'programlisting', 
                        'itemizedlist', 'orderedlist', 'command', 'anchor', 'link', 'xref' ]

    def subserialize(self, node):
        """Serializes all the children of a node."""
        ret = ""
        if node:
            child = node.children
            while child:
                if child.type=='element' and not self.tag_is_inline(child.name):
                    DEBUG("Warning: tag '%s' is not known as inline in '%s', but still treated as such." % (child.name, node.name))
                ret += child.serialize('utf-8').decode('utf-8')
                child = child.next
        return ret

    def set_tags(self, xmlnode, unit):
        """Derives what tags should be set on "xmlnode" represented with "unit".

        It sets preformatted/spacepreserving, translatable, needswork... tags."""
        mytags = []
        if xmlnode.name in ["imagedata"]:
            mytags.append("file")

        if xmlnode.name in ["programlisting", "literal", "synopsis"]:
            mytags.append("spacepreserve")

        if xmlnode.name in ["part", "chapter", "section", "section1", "reference", "refentry", "refsect1"]:
            mytags.append("branchpoint")

        if xmlnode.name in ["title"]:
            mytags.append("singleline")

        if xmlnode.name in ["publishername", "firstname", "surname", "year", "holder", "email",
                            "refentrytitle", "manvolnum", "refmiscinfo", "refname", "refpurpose"]:
            mytags.append("plaintext")

        if self.get_node_type(xmlnode) not in ['string', 'struct', 'sequence']:
            mytags.append("unsupported")


        for tag in mytags:
            newtag = DS.UnitTag(Unit=unit, Tag=tag)

    def serialize_attributes(self, xmlnode, unit):
        """Serializes node attributes into a unit of struct type.

        Returns reference to constructed unit or None in case of a problem."""
        if not xmlnode or xmlnode.type != 'element': return None

        attrs = DS.Unit( Document = unit.Document,
                         Descriptor = 'ATTRIBUTES',
                         Type = 'struct',
                         Revision = 1,
                         RelatedTo = unit )

        if xmlnode.properties:
            for p in xmlnode.properties:
                if p.type == 'attribute':
                    DEBUG(p.name + ":=" + self.subserialize(p))
                    mydata = DS.UnitData( Unit = attrs,
                                          FieldName = p.name,
                                          Content = self.subserialize(p),
                                          Hints = "" )

        return attrs
        

    def get_node_type(self, node):
        """Return node type for DB storage.

        Returns one of "string", "struct" or "sequence"."""
        if node.type == "element":
            if node.name in ["title", "para", "programlisting", "synopsis", "simpara",
                             "variablelist", "itemizedlist", "orderedlist", "table",
                             "refentrytitle", "manvolnum", "refmiscinfo", "refname", "refpurpose",
                             "publishername", "firstname", "surname", "year", "holder", "email", "releaseinfo" ]:
                return "string"
            elif node.name in [ #"itemizedlist", "variablelist", "unorderedlist",
                "figure", "mediaobject", "screenshot",
                "book", "article", "part", "reference", "partintro",
                "bookinfo", "articleinfo", "legalnotice",
                "chapter", "preface", "section", "index",
                "refentry", "refsect1", "refsect2", "refsect3",
                "refsynopsisdiv", "refnamediv", "refmeta", 
                "sect1", "sect2", "sect3", "sect4", "sect5", "highlights",
                "authorgroup", "revhistory",
                "author", "copyright", "publisher" ]:
                return "sequence"
            elif node.name in ["imagedata", "anchor"]:
                return "struct"
            else:
                DEBUG("Warning: element %s is unknown." % (node.name))
                return None
        elif node.type == "text":
            DEBUG("Warning: unhandled text node")
            return None
        else:
            return None
        
    def process_node(self, xmlnode, baseunit):
        """Processes XML node and constructs DB units out of it."""
        if xmlnode.type == 'element':
            mytype = self.get_node_type(xmlnode)
            myelem = DS.Unit( Document = self.doc,
                              Descriptor = xmlnode.name,
                              Type = mytype,
                              Revision = 1,
                              RelatedTo = baseunit )
            myattrs = self.serialize_attributes(xmlnode, myelem)
            mytags = self.set_tags(xmlnode, myelem)
            
            INFO("Type of %s is %s" % (xmlnode.name, mytype))
            if mytype == 'string':
                mydata = DS.UnitData( Unit = myelem,
                                      FieldName = "TEXT",
                                      Content = self.subserialize(xmlnode) )
            elif mytype == 'struct':
                child = xmlnode.children
                while child:
                    self.process_node(child, myelem)
                    child = child.next
            elif mytype == 'sequence':
                child = xmlnode.children
                seq = 0
                while child:
                    newelem = self.process_node(child, myelem)
                    if newelem:
                        newelem.Sequence = seq
                        seq += 1
                    
                    child = child.next
            else:
                # Unsupported tag, store it as a string
                myelem.Type = 'string'
                mydata = DS.UnitData( Unit = myelem,
                                      FieldName = "TEXT",
                                      Content = self.subserialize(xmlnode) )

        else:
            if not xmlnode.isBlankNode():
                DEBUG("Warning: ignored possibly unignorable %s node: '%s'" % (xmlnode.type, xmlnode.content))
            else:
                INFO("Ignoring blank %s node" % (xmlnode.type))
            myelem = None

        return myelem

    def parse_book(self, root, doc = None):
        if not doc: doc = self.doc.BaseUnit

        self.process_node(root, doc)

#Import = DocBookReader(file = 'sample/simple.xml')
#Import2 = DocBookReader(file = 'gobject.simple/doks.xml')
#Import3 = DocBookReader(file = 'gobject/doks.xml')
#Import4 = DocBookReader(file = 'sample/user-guide.xml')

if __name__ == '__main__':
    import sys
    Import = DocBookReader(file = sys.argv[1])

