#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""Displays statistics for a document.

Statistics include listing units with "needswork" tag for now, along with commentary."""

import cgi
import cgitb; cgitb.enable()
from Cheetah.Template import Template

from document import *
import docstorage
import login

def unit_has_tag(unit, tag):
    for utag in unit.Tags:
            if utag.Tag == tag:
                return 1
    return 0
    

def get_needswork_tags(docid):
    allunits = docstorage.Unit.select(docstorage.Unit.q.DocumentID==int(docid)) #, orderBy='ID', orderBy='Sequence')
    needswork = []
    for unit in allunits:
        if unit_has_tag(unit, 'needswork'):
            editunit = EditingUnit(id=unit.id)

            # lets find parent "branchpoint" node to use in links
            if unit_has_tag(unit,'branchpoint'):
                editunit.section = editunit # recursive indication that we need to show ourselves
            else:
                parent = unit.RelatedTo
                last = unit
                while parent.RelatedTo and not unit_has_tag(parent,'branchpoint'):
                    last = parent
                    parent = parent.RelatedTo
                #parent = { "id": 0 }
                if not parent.RelatedTo:
                    editunit.parent_is_document = 1
                    editunit.section = { "id" : parent.Document.id,
                                         "title": EditingUnit(id=last.id).get_my_title() }
                else:
                    editunit.parent_is_document = 0
                    editunit.section = EditingUnit(id=parent.id)
                needswork.append(editunit)

    return needswork
                
        

print "Content-type: text/html; charset=UTF-8\n"

form = cgi.FieldStorage()

docid = None
unitid = None

if form.has_key("document"):
    docid = form.getlist("document")[0]
    html = Template(file="templates/statistics.tmpl")
    html.logged_in = login.IsLoggedIn()
    doc = Document(id = docid)
    if doc:
        html.units = get_needswork_tags(docid)
        html.document = doc
        print html
    else:
        print "Unknown document requested."
else:
    print "Hi there"


