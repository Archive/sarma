#!/usr/bin/env python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from document import *

def export_node(node):
    out = ""
    attrs = ""
    if node and node.dbunit and node.dbunit.Values.has_key('ATTRIBUTES'):
        allA = node.dbunit.Values['ATTRIBUTES']
        for at, val in allA.items():
            attrs += " %s=\"%s\"" % (at, val)
    if (node.type == 'sequence' or node.type == 'struct') and node.descriptor != 'ATTRIBUTES':
        out = "<%s%s>" % (node.descriptor, attrs)
        for key, value in node.list_subunits().items():
            out += export_node(value)
        out += "</%s>" % (node.descriptor)
    elif node.type == 'string':
        out += "<%s%s>" % (node.descriptor, attrs) + node.contents + "</%s>" % (node.descriptor)
    else:
        out += ""

    return out
    

def export(id):
    doc = Document(id=id)
    if doc:
        root = doc.Data
        out = ""
        for key, value in root.list_subunits().items():
            out += export_node(value)
        return out

    return ""

if __name__=="__main__":
    import sys
    print export(id=sys.argv[1]).encode('utf-8')
    
    
