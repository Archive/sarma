#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""Remote function call interface for AJAX support.

Accepts parameters "method", "unit", "requesttype", "value"."""

import cgi, sys
import cgitb; cgitb.enable(logdir="/tmp/cgi", format="text")
from document import *
import login

print "Content-type: text/html; charset=UTF-8\n"


import libxml2
import libxslt

def transform_html2db(html):
    node = 'prda'
    styledoc = libxml2.parseFile("xslt/html2db.xsl")
    style = libxslt.parseStylesheetDoc(styledoc)
    contents = None
    if html:
        html = html.replace('<br>', '<br/>')
        html = html.replace('<hr>', '<hr/>')
        myxml = '<?xml version="1.0" encoding="utf-8"?>\n<%s>' % (node) + html.encode('utf-8') + '</%s>' % (node)
        #f = open("/tmp/log", "w"); f.write(myxml); f.close()
        doc = libxml2.parseMemory(myxml, len(myxml))
        result = style.applyStylesheet(doc, None)
        contents = style.saveResultToString(result)
        doc.freeDoc()
        result.freeDoc()

    style.freeStylesheet()
    return contents


def edit(unitid, oldcontents):
    """Replaces contents of unitid with newcontents, and increases a revision number."""
    unit = EditingUnit(id=unitid)
    if not login.CanEdit(unitid):
        return u"-2\nUnauthorised access. Check your privileges"
    if unit and unit.type == 'string':
        newcontents = transform_html2db(oldcontents.decode('utf-8'))
        import docstorage

        for field in unit.dbunit.Data:
            if field.FieldName == 'TEXT':
                change = docstorage.UnitChange(Unit = unit,
                                               FromRevision = unit.revision,
                                               NewRevision = unit.revision+1,
                                               BaseRevision = unit.revision,
                                               Approved = 1,
                                               ChangeType = 'edit',
                                               Parameter = field.Content,
                                               Editor = login.IsLoggedIn().User.id)
                field.Content = newcontents
                unit.dbunit.Revision += 1
            
        return u"0\nSet %s from '%s' to: '%s'" % (unitid, oldcontents.decode('utf-8'), newcontents.decode('utf-8'))
    else:
        return "-1\nTrying to modify a non-string/%s %s." % (unit.type, unitid)


def cleartag(unitid, tag):
    """Clears a tag "tag" for unit with ID "unitid"."""
    if not login.CanEdit(unitid):
        return u"-2\nUnauthorised access. Check your privileges"
    unit = EditingUnit(id=unitid)
    if unit:
        for mytag in unit.dbunit.Tags:
            if mytag.Tag == tag:
                tagid = mytag.id
                import docstorage
                docstorage.UnitTag.delete(tagid)
                return "0\nRemoved."
        return "0\nTag not set."
    else:
        return "-1\nCan't access unit with ID %s." % (unitid)
    
def settag(unitid, tag):
    """Sets a tag "tag" for unit with ID "unitid"."""
    if not login.CanEdit(unitid):
        return u"-2\nUnauthorised access. Check your privileges"
    unit = EditingUnit(id=unitid)
    if unit:
        if unit.tags.has_key(tag):
            return "0\nAlready set."
        import docstorage
        newtag = docstorage.UnitTag(Unit=unit.dbunit, Tag=tag)
        return u"0\nSet tag %s for '%s'" % (tag, unit.descriptor)
    else:
        return "-1\nCan't access unit with ID %s." % (unitid)



def addcomment(unitid, comment):
    """Adds a comment by currently logged in user tounit with ID "unitid"."""
    check = login.IsLoggedIn()
    
    if not check.User:
        return u"-2\nUnauthorised access. Please log in first."
    unit = EditingUnit(id=unitid)
    if unit:
        import docstorage
        newcomment = docstorage.UnitComment(Unit=unit.dbunit, Poster=check.User, Comment=comment)
        if newcomment:
            return u"0\nAdded comment."
        else:
            return u"-1\nThere was a problem posting comment."
    else:
        return u"-1\nCan't access unit with ID %s." % (unitid)


form = cgi.FieldStorage()

import sys

if form.getlist("method"):
    method = form.getlist("method")[0]
    unit = form.getlist("unit")[0]
    cont = form.getlist("value")[0]
    if method=='edit':
        print >>sys.stdout, edit(unit, cont).encode('utf-8')
    elif method=='settag':
        print >>sys.stdout, settag(unit, cont).encode('utf-8')
    elif method=='cleartag':
        print >>sys.stdout, cleartag(unit, cont).encode('utf-8')
    elif method=='addcomment':
        print >>sys.stdout, addcomment(unit, cont).encode('utf-8')
