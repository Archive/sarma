<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/">
<xsl:apply-templates/> 
</xsl:template>

<xsl:template match="para"><xsl:apply-templates/></xsl:template>

<xsl:template match="emphasis|title|simpara|function|filename|command|parameter|guibutton|guilabel|guimenu|guisubmenu|guimenuitem|application|keycap|literal|type"><span><xsl:attribute name="class">
      <xsl:value-of select="name(.)"/></xsl:attribute><xsl:apply-templates/></span>
   </xsl:template>

<xsl:template match="programlisting"><pre class="programlisting"><xsl:value-of select="."/></pre></xsl:template>

<xsl:template match="synopsis"><pre class="synopsis"><xsl:apply-templates/></pre></xsl:template>

<xsl:template match="figure/mediaobject/imageobject[1]/imagedata">
  <img>
    <xsl:attribute name="src">
      figures/<xsl:value-of select="@fileref"/>
    </xsl:attribute>
  </img>
</xsl:template>


<xsl:template match="text()"><xsl:value-of select="."/></xsl:template>

<xsl:template match="listitem/para[position()&lt;last()]"><xsl:apply-templates/><br /></xsl:template>
<xsl:template match="listitem/simpara[position()&lt;last()]"><xsl:apply-templates/><br /></xsl:template>
<xsl:template match="listitem/simpara"><xsl:apply-templates/></xsl:template>

<xsl:template match="itemizedlist"><ul><xsl:apply-templates match="listitem"/></ul></xsl:template>
<xsl:template match="itemizedlist/listitem"><li><xsl:apply-templates/></li></xsl:template>

<xsl:template match="orderedlist"><ol><xsl:apply-templates match="listitem"/></ol></xsl:template>
<xsl:template match="orderedlist/listitem"><li><xsl:apply-templates/></li></xsl:template>

<xsl:template match="variablelist"><dl><xsl:apply-templates match="varlistentry"/></dl></xsl:template>
<xsl:template match="variablelist/varlistentry/term"><dt><xsl:apply-templates/></dt></xsl:template>
<xsl:template match="variablelist/varlistentry/listitem"><dd><xsl:apply-templates/></dd></xsl:template>

<xsl:template match="keycombo"><span class="keycombo"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="keycombo/keycap[position()&lt;last()]"><span class="keycap"><xsl:value-of select="."/></span>+</xsl:template>
<xsl:template match="keycombo/keycap[position()=last()]"><span class="keycap"><xsl:value-of select="."/></span></xsl:template>

<xsl:template match="keycap"><span class="keycap"><xsl:value-of select="."/></span></xsl:template>




<xsl:template match="table"><table><xsl:apply-templates/></table></xsl:template>
<xsl:template match="table/title"><center><xsl:apply-templates/></center></xsl:template>
<xsl:template match="table/thead"><thead><xsl:apply-templates/></thead></xsl:template>
<xsl:template match="table/tbody"><tbody><xsl:apply-templates/></tbody></xsl:template>
<xsl:template match="table//row"><tr><xsl:apply-templates/></tr></xsl:template>
<xsl:template match="thead//entry"><th><xsl:apply-templates/></th></xsl:template>
<xsl:template match="tbody//entry"><td><xsl:apply-templates/></td></xsl:template>



<xsl:template match="anchor"><a class="anchor">
    <xsl:attribute name="name"><xsl:value-of select="./@id"/></xsl:attribute>
    <img src="tags/stock_anchor.png" />
  </a></xsl:template>
<xsl:template match="link"><a class="link">
    <xsl:attribute name="href">#<xsl:value-of select="./@linkend"/></xsl:attribute>
    <xsl:apply-templates/>
  </a></xsl:template>

<xsl:template match="xref"><a class="link">
    <xsl:attribute name="href">#<xsl:value-of select="./@linkend"/></xsl:attribute>
    <xsl:attribute name="title">Reference to anchor "<xsl:value-of select="./@linkend"/>"</xsl:attribute>
    <code>#<xsl:value-of select="./@linkend"/></code>
  </a></xsl:template>


</xsl:stylesheet>
