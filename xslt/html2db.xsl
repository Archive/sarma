<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" encoding="utf-8" />

<xsl:template match="/">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="root"><xsl:apply-templates/></xsl:template>

<xsl:template match="p"><para><xsl:apply-templates/></para></xsl:template>

<xsl:template match='span[@class="emphasis"]|span[@class="title"]|span[@class="simpara"]|span[@class="function"]|span[@class="filename"]|span[@class="command"]|span[@class="parameter"]|span[@class="guibutton"]|span[@class="guilabel"]|span[@class="guimenu"]|span[@class="guisubmenu"]|span[@class="guimenuitem"]|span[@class="application"]|span[@class="keycap"]|span[@class="keycombo"]|span[@class="literal"]|span[@class="type"]'><xsl:element name="{@class}"><xsl:apply-templates/></xsl:element></xsl:template>

<xsl:template match='span[@class="keycombo"]/text()' /> <!-- ignore + we add in db2html.xsl -->

<xsl:template match='pre[@class="programlisting"]'><programlisting><xsl:value-of select="."/></programlisting></xsl:template>
<xsl:template match='pre[@class="synopsis"]'><synopsis><xsl:value-of select="."/></synopsis></xsl:template>

<xsl:template match="img">
  <figure>
    <xsl:if test="@title"><title><xsl:value-of select="@title"/></title></xsl:if>
    <mediaobject><imageobject><imagedata>
    <xsl:attribute name="fileref">
      figures/<xsl:value-of select="@src"/>
    </xsl:attribute></imagedata></imageobject></mediaobject>
  </figure>
</xsl:template>

<xsl:template match="text()"><xsl:value-of select="."/></xsl:template>

<xsl:template match="li"><para><xsl:apply-templates/></para></xsl:template>

<xsl:template match="ul"><itemizedlist><xsl:apply-templates match="li"/></itemizedlist></xsl:template>
<xsl:template match="ul/li"><listitem><xsl:apply-templates/></listitem></xsl:template>

<xsl:template match="ol"><orderedlist><xsl:apply-templates match="li"/></orderedlist></xsl:template>
<xsl:template match="ol/li"><listitem><xsl:apply-templates/></listitem></xsl:template>

<!-- FIXME: varlistentry must contain "term" and "listitem" -->
<xsl:template match="dl"><variablelist><xsl:apply-templates match="li"/></variablelist></xsl:template>
<xsl:template match="dl//dt"><term><xsl:apply-templates/></term></xsl:template>
<xsl:template match="dl//dd"><listitem><xsl:apply-templates/></listitem></xsl:template>
<xsl:template match="variablelist/varlistentry/listitem"><dd><xsl:apply-templates/></dd></xsl:template>


<xsl:template match="table"><table><xsl:apply-templates/></table></xsl:template>
<xsl:template match="table/center"><title><xsl:apply-templates/></title></xsl:template>
<xsl:template match="table/thead"><thead><xsl:apply-templates/></thead></xsl:template>
<xsl:template match="table/tbody"><tbody><xsl:apply-templates/></tbody></xsl:template>
<xsl:template match="table//tr"><row><xsl:apply-templates/></row></xsl:template>
<xsl:template match="table//th"><entry><xsl:apply-templates/></entry></xsl:template>
<xsl:template match="table//td"><entry><xsl:apply-templates/></entry></xsl:template>



<xsl:template match="a[@class='anchor']">
  <anchor><xsl:attribute name="id"><xsl:value-of select="./@name"/></xsl:attribute></anchor>
</xsl:template>

<!-- FIXME: special handling for xref needed -->
<xsl:template match="a[@class='link']"><link>
    <xsl:attribute name="linkend">#<xsl:value-of select="./@href"/></xsl:attribute>
    <xsl:apply-templates/></link></xsl:template>



</xsl:stylesheet>
