#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""Allows viewing and editing a document as stored in database."""

from common import *
import docstorage
import libxml2
import libxslt

class EditingUnit:
    """Interface to all the operations on a single unit.

    It provides "type" and "contents" attributes, and
    methods used to do any work on the unit."""
    def __init__(self, id = None, depth = -1, type = None, Descriptor = '', Document = 0, Revision = 1, RelatedTo = 0, contents = ''):
        self.id = id
        self.dbunit = docstorage.Unit.get(id)
        if self.dbunit:
            self.type = self.dbunit.Type
            self.descriptor = self.dbunit.Descriptor
            self.tags = { }
            for tag in self.dbunit.Tags:
                self.tags[tag.Tag] = 1
            self.revision = self.dbunit.Revision
        else:
            if not type in ['string', 'struct', 'sequence']:
                raise ValueError, 'You must instantiate EditingUnit with existing database unit or correct type.'
            else:
                self.dbunit = docstorage.Unit(Type=type,
                                              Descriptor=Descriptor,
                                              Document=Document,
                                              Revision=Revision,
                                              RelatedTo=RelatedTo)
                self.type = type
                self.id = self.dbunit.id
                self.revision = 0
                # FIXME: Add empty unitdata as well

        #print "<br/>type: ", self.type, self.dbunit.Descriptor, self.id
        self.content_present = 0
        if   self.type == 'string':   self.contents = ''
        elif self.type == 'struct':   self.contents = { }
        elif self.type == 'sequence': self.contents = [ ]
        
        self.populate_contents(depth)

    def transform_db2html(self):
        node = self.descriptor.encode('utf-8')
        styledoc = libxml2.parseFile("xslt/db2html.xsl")
        style = libxslt.parseStylesheetDoc(styledoc)
        if self.contents:
            myxml = '<?xml version="1.0" encoding="utf-8"?>\n<%s>' % (node) + self.contents.encode('utf-8') + '</%s>' % (node)
            doc = libxml2.parseMemory(myxml, len(myxml))
            result = style.applyStylesheet(doc, None)
            self.html_contents = style.saveResultToString(result)
            doc.freeDoc()
            result.freeDoc()
        style.freeStylesheet()


    def transform_html2db(self, html):
        node = 'root'
        styledoc = libxml2.parseFile("xslt/html2db.xsl")
        style = libxslt.parseStylesheetDoc(styledoc)
        contents = None
        if html:
            myxml = '<?xml version="1.0" encoding="utf-8"?>\n<%s>' % (node) + html.encode('utf-8') + '</%s>' % (node)
            #print "<textarea cols='80' rows='8'>" + myxml + "</textarea>"
            doc = libxml2.parseMemory(myxml, len(myxml))
            result = style.applyStylesheet(doc, None)
            contents = style.saveResultToString(result)
            print "<textarea cols='80' rows='8'>" + myxml + "</textarea>"
            doc.freeDoc()
            result.freeDoc()

        style.freeStylesheet()
        return contents.decode('utf-8')
            

    def get_my_title(self):
        """Tries to get a title for current node."""
        try:
            return self.title
        except:
            if self.type in ['struct', 'sequence']:
                if self.type == 'struct':
                    for elem in docstorage.Unit.select("related_id='%s'" % (str(self.dbunit.id)), orderBy = docstorage.Unit.q.Sequence)[:3]:
                        if (elem.Descriptor == 'title' or elem.Descriptor == 'refentrytitle') and elem.Type=='string':
                            myelem = EditingUnit(id=elem.id, depth=0)
                            self.title = myelem.contents
                            return myelem.contents
                        elif (elem.Descriptor == 'bookinfo' or elem.Descriptor == 'articleinfo'):
                            myelem = EditingUnit(id=elem.id, depth=0)
                            self.title = myelem.get_my_title()
                            return self.title
                            
                elif self.type == 'sequence':
                    for elem in docstorage.Unit.select("related_id='%s'" % (str(self.dbunit.id)), orderBy = docstorage.Unit.q.Sequence)[:3]:
                        if (elem.Descriptor == 'title' or elem.Descriptor == 'refentrytitle') and elem.Type=='string':
                            myelem = EditingUnit(id=elem.id, depth=0)
                            self.title = myelem.contents
                            return myelem.contents
                        elif (elem.Descriptor == 'bookinfo' or elem.Descriptor == 'articleinfo'):
                            myelem = EditingUnit(id=elem.id, depth=0)
                            self.title = myelem.get_my_title()
                            return self.title

            return None

    def populate_contents(self, depth=-1):
        if depth==0 and self.type != 'string':
            return

        if self.type == 'struct':
            self.contents = { }
            self.content_present = 1
            for elem in docstorage.Unit.select("related_id='%s'" % (str(self.dbunit.id)), orderBy = docstorage.Unit.q.Sequence):
                fieldname = elem.Descriptor
                myelem = EditingUnit(id=elem.id, depth=depth-1)
                if fieldname in self.contents:
                    DEBUG("Warning: '%s' is redefined inside '%s'." % (fieldname, self.dbunit.Descriptor))
                self.contents[fieldname] = myelem
            #print self.contents
        elif self.type == 'sequence':
            self.contents = [ ]
            self.content_present = 1
            for elem in docstorage.Unit.select("related_id='%s'" % (str(self.dbunit.id)), orderBy = docstorage.Unit.q.Sequence):
                myelem = EditingUnit(id = elem.id, depth=depth-1)
                self.contents.append(myelem)
            #print self.contents
        elif self.type == 'string':
            self.content_present = 1
            if self.dbunit.Values.has_key('TEXT'):
                self.contents = self.dbunit.Values['TEXT']
            else:
                self.contents = ''
            self.transform_db2html()
        else:
            DEBUG("Warning: unknown type '%s' for database unit %s: switching to sequence." % (self.type, str(self.dbunit.id)))
            self.type = 'sequence'
            self.content_present = 0
            self.contents = []


    def list_subunits(self):
        """Always returns a dictionary for whatever a unit contains.

        For strings, it returns dictionary { "0": string}, for sequences,
        it transforms array into dictionary with indexes used for keys, and
        for structs, it simply returns a dictionary.

        If the element data hasn't been read, it is read lazily from database."""

        if not self.content_present:
            DEBUG("Warning: this element has not been populated, populating it...")
            self.populate_contents(1)
            if not self.content_present: #"contents" not in dir(self):
                DEBUG("Error: can't populate element %s." % (str(self.id)))
                return { }

        if self.dbunit:
            if self.type == 'struct':
                return self.contents
            elif self.type == 'sequence':
                mycont = { }
                for i in range(0, len(self.contents)):
                    mycont[i] = self.contents[i]
                return mycont
            elif self.type == 'string':
                return { 0 : self.contents }
            else:
                DEBUG("Warning: Listing subunits of node with inappropriate type '%s'." % (self.type))
                return None
        else:
            DEBUG("Warning: EditingUnit is not available in a database.")

        return { }
            

class Document:
    """Container class for editable document."""
    def __init__(self, id, depth=-1):
        mydoc = docstorage.Document.get(id)
        if not mydoc:
            DEBUG("Warning: requested nonexistant document with ID %s." % (str(id)))
            return None
        
        self.Doc = mydoc
        self.Language = mydoc.Language
        self.Type = mydoc.Type
        self.id = mydoc.id
        self.Data = EditingUnit(id = mydoc.BaseUnit.id, depth=depth)
        self.Title = None
        self.Revision = self.Data.revision
        for elem in docstorage.Unit.select("related_id='%s'" % (str(mydoc.BaseUnit.id)), orderBy = docstorage.Unit.q.Sequence)[:2]:
            if (elem.Descriptor == 'book' or elem.Descriptor == 'article') and elem.Type=='sequence':
                myelem = EditingUnit(id=elem.id, depth=0)
                self.Title = myelem.get_my_title()
        
