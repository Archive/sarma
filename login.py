#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
# Copyright (c) 2005 Danilo Šegan <danilo@gnome.org>.
#
# This file is part of Sarma.
#
# Sarma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Sarma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sarma; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

"""Displays document from a database as HTML."""

class PrivilegeCheck:
    def __init__(self, username, password):
        self.privileges = {}

        import docstorage
        ok = 0
        for user in docstorage.User.select("""full_name='%s'""" % (username)):
            if user.Password == password:
                ok = user
            break
        if not ok:
            self.User = None
            return
        else:
            self.User = ok
            # get all the privileges
            for privs in ok.Privileges:
                self.privileges['read'] = privs.Reading
                self.privileges['write'] = privs.Writing
                self.privileges['edit'] = privs.Editing
                self.privileges['meta'] = privs.Metadata

def IsLoggedIn():
    import os
    if os.environ.has_key("HTTP_COOKIE"):
        import Cookie
        jar = Cookie.SimpleCookie(os.environ["HTTP_COOKIE"])
        if jar.has_key('username') and jar.has_key('password'):
            check = PrivilegeCheck(jar['username'].value, jar['password'].value)
            if check.User: return check
    return 0

def CanEdit(id):
    #import document
    #elem = EditingUnit(id=id)
    who = IsLoggedIn()
    if who and who.privileges['edit']:
        return 1
    else:
        return 0
    

def LogOut():
    import Cookie, os
    C = Cookie.SimpleCookie()
    C['password'] = ""
    print C
    os.environ['HTTP_COOKIE'] = ''
    return

if __name__=="__main__":
    import cgi
    import cgitb; cgitb.enable()
    from Cheetah.Template import Template
    from document import *

    form = cgi.FieldStorage()

    if form.has_key("loginname") and form.has_key("loginpw"):
        # clear cookies
        LogOut()
        # Submitted data
        user = form.getlist("loginname")[0]
        pswd = form.getlist("loginpw")[0]

        check = PrivilegeCheck(user,pswd)
        if check.User:
            import Cookie
            C=Cookie.SimpleCookie()
            C["username"] = user
            C["password"] = pswd
            print C
            print "Content-type: text/html; charset=UTF-8"
            print "Location: list.py\n"
        else:
            print "Content-type: text/html; charset=UTF-8\n"

            html = Template(file="templates/login.tmpl")
            html.user = user
            html.password = pswd
            html.failed = "Login failed"
            html.logged_in = IsLoggedIn()
            print html
    else:
        print "Content-type: text/html; charset=UTF-8\n"
        html = Template(file="templates/login.tmpl")
        html.user = "demo"
        html.password = "demo"
        html.failed = 0
        html.logged_in = IsLoggedIn()
        print html


